﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace FirstApp
{

    class Program
    {
        static void CompressFiles(string path)
        {
            try
            {
                string[] dirs = Directory.GetFiles(path);
                int totalCount = dirs.Length;
                int i = 0;
                Console.WriteLine("{0} files found for processing..", dirs.Length);

                foreach (string file in dirs)
                {
                    try
                    {
                        FileInfo fileInfo = new FileInfo(file);
                        string newfile = path + "\\compressed_" + fileInfo.Name;

                        if (fileInfo.Extension == ".exe") { continue; }

                        Image image = Image.FromFile(file);
                        ResizeOrigImg(image, 1440, 900, newfile);
                        
                        //garbage collection and delete old file 
                        image.Dispose();
                        File.Delete(file);
                        
                        // Progress bar
                        DrawTextProgressBar(i++, totalCount);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Файл {0} не является изображением", file);
                        continue;
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Процесс не удался: {0}. Обратитесь к администратору.", e.ToString());
                Console.ReadKey();
            }
        }

        static void DrawTextProgressBar(int progress, int total)
        {
            //draw empty progress bar
            Console.CursorLeft = 0;
            Console.Write("["); //start
            Console.CursorLeft = 32;
            Console.Write("]"); //end
            Console.CursorLeft = 1;
            float onechunk = 30.0f / total;

            //draw filled part
            int position = 1;
            for (int i = 0; i < onechunk * progress; i++)
            {
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.CursorLeft = position++;
                Console.Write(" ");
            }

            //draw unfilled part
            for (int i = position; i <= 31; i++)
            {
                Console.BackgroundColor = ConsoleColor.Green;
                Console.CursorLeft = position++;
                Console.Write(" ");
            }

            //draw totals
            Console.CursorLeft = 35;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write(progress.ToString() + " of " + total.ToString() + " "); //blanks at the end remove any excess
        }

        public static void ResizeOrigImg(Image image, int nWidth, int nHeight, string newPath)
        {
            int newWidth, newHeight;
            var coefH = (double)nHeight / (double)image.Height;
            var coefW = (double)nWidth / (double)image.Width;
            if (coefW >= coefH)
            {
                newHeight = (int)(image.Height * coefH);
                newWidth = (int)(image.Width * coefH);
            }
            else
            {
                newHeight = (int)(image.Height * coefW);
                newWidth = (int)(image.Width * coefW);
            }

            Image result = new Bitmap(newWidth, newHeight);
            using (var g = Graphics.FromImage(result))
            {
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                g.DrawImage(image, 0, 0, newWidth, newHeight);
                g.Dispose();
            }
            result.Save(newPath);
        }

        static void Main(string[] args)
        {
            // Получаем PWD
            string pwd = Environment.CurrentDirectory;

            // Сжимаем файлы
            CompressFiles(pwd);
        }
    }

}
